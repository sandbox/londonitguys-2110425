QuickTabs ZURB Foundation
=========================

INTRODUCTION
------------

QuickTabs ZURB Foundation provides a new renderer plugin to the QuickTabs module,
allowing you to display any Quicktabs using ZURB Foundation 4.x the Sections
component.

Please refer to the Zurb Foundation 4.x's documentation for more information:
http://foundation.zurb.com/docs/components/section.html.


INSTALLATION
------------

 * Install as usual, see http://drupal.org/node/70151 for further information.


REQUIREMENTS
------------

 * Quick Tabs.
 * Foundation 4.x based themes, or themes that use the Zurb Foundation framework
   in some manner.


CONFIGURATION
-------------
 1. Enable Quicktabs ZURB Foundation in module list located at
    administer > structure > modules.

 2. Go to admin/structure/quicktabs and create a Quicktabs Instance, see the
    QuickTabs module for more information.

 3. When choosing the Quicktabs renderer, select "zurb_foundation".

 4. Select a ZURB section types.


Options
=======

At the moment QuickTabs ZURB Foundation supports the following Sections:
  - Accordion
  - Tabs
  - Vertical Tabs

Please fill in a support request if you think more ZURB Foundation Sections types
should be added.
