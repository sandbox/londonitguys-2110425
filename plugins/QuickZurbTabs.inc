<?php

/**
 * @file
 * QuickTabs plugin using the ZURB Foundation framework.
 */

/**
 * Renders the content using ZURB Foundation 4.x components.
 */
class QuickZurbTabs extends QuickRenderer {

  public static function optionsForm($qt) {

    // ZURB Foundation sections types.
    // Not all the ZURB section types are supported here, as I am not sure it
    // makes sens to use Quicktabs for Horizontal Nav/Vertical Nav for example.
    // Feedbacks welcome...
    $section_types = array(
      'auto' => t('Auto'),
      'accordion' => t('Accordion'),
      'tabs' => t('Tabs'),
      'vertical-tabs' => t('Vertical Tabs'),
    );

    $form = array(
      'section_type' => array(
        '#type' => 'radios',
        '#title' => t('Zurb section type'),
        '#options' => $section_types,
        '#description' => t('The Zurb Foundation framwork provides different section styles, see the <a href="@zurb-doc" title="ZURB Foundation documentation">ZURB Foundation documentation page</a> for more info.', array('@zurb-doc' => 'http://foundation.zurb.com/docs/components/section.html')),
        '#default_value' => isset($qt->renderer) && $qt->renderer == 'zurb_foundation' && isset($qt->options['section_type']) ? $qt->options['section_type'] : 'tabs',
      ),
      'deep_linking' => array(
        '#type' => 'checkbox',
        '#title' => 'Deep Linking to sections of content',
        '#description' => t('Deep linking allows visitors to visit a predefined URL with a hash that points to a particular section of the content. see the <a href="@zurb-doc" title="ZURB Foundation documentation">ZURB Foundation documentation page</a> for more info.', array('@zurb-doc' => 'http://foundation.zurb.com/docs/components/section.html')),
        '#default_value' => (isset($qt->renderer) && $qt->renderer == 'zurb_foundation' && isset($qt->options['deep_linking']) && $qt->options['deep_linking']),
      ),
      'callback' => array(
        '#type' => 'textfield',
        '#title' => 'JavaScript Callback function name',
        '#description' => t('This function will be called when a section is selected, e.g. adjust_titles_position. see the <a href="@zurb-doc" title="ZURB Foundation documentation">ZURB Foundation documentation page</a> for more info.', array('@zurb-doc' => 'http://foundation.zurb.com/docs/components/section.html')),
        '#default_value' => isset($qt->renderer) && $qt->renderer == 'zurb_foundation' && isset($qt->options['callback']) ? $qt->options['callback'] : '',
      ),
    );
    return $form;
  }

  public function render() {
    $quickset = $this->quickset;
    $qt_name = $quickset->getName();
    $active_tab = $quickset->getActiveTab();
    $settings = $this->quickset->getSettings();
    $options = $settings['options'];

    $render_array = array(
      'content' => array(
        '#theme' => 'qt_zurb_foundation',
        '#options' => array(
          'attributes' => array(
            'id' => 'section-' . $qt_name,
            'class' => array('section-container', $options['section_type']),
          ),
          'section_type' => $options['section_type'],
          'deep_linking' => $options['deep_linking'],
          'callback' => $options['callback'],
          'active_tab' => $active_tab,
        ),
        'sections' => array(),
      ),
    );
    foreach ($quickset->getContents() as $key => $tab) {
      if (!empty($tab)) {

        // Tab title. If we use l() here or a render array of type 'link',
        // the '#' symbol will be escaped.
        $href = 'section-' . $qt_name . $key;
        $title = '<p class="title" data-section-title><a href="#' . $href . '">' . check_plain($quickset->translateString($tab->getTitle(), 'tab', $key)) . '</a></p>';

        // If Deep linking is set to true add the data-slug attribute.
        $attribs = array();
        if ($options['deep_linking']) {
          $attribs = array(
            'class' => array('content'),
            'data-slug' => $href,
          );
        }
        else {
          $attribs = array(
            'class' => array('content'),
          );
        }

        // Section content.
        $render_array['content']['sections'][] = array(
          '#prefix' => $title . '<div ' . drupal_attributes($attribs) . 'data-section-content>',
          '#suffix' => '</div>',
          'content' => $tab->render(),
        );
      }
    }
    return $render_array;
  }
}
